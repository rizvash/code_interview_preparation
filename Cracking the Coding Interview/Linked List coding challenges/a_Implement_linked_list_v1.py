#  https://github.com/kojino/data-structures-algorithms/tree/master/linked_list
#  https://medium.com/@kojinoshiba/data-structures-in-python-series-1-linked-lists-d9f848537b4d


'''
Implement a linked list v1
'''
class Node:
    def __init__(self,val):
        self.val = val
        self.next = None # the pointer initially points to nothing