def fizzbuzz(number):
    if number % 3 == 0 and number % 5 == 0:
        return 'FizzBuzz'
    elif number % 3 == 0:
        return 'Fizz'
    elif number % 5 == 0:
        return 'Buzz'
    else:
        return number

for number in range(1, 101):
    print (fizzbuzz(number))


"""
# Чуть более лаконичное решение - в нем столько же строк, сколько и в предыдущем
# но оно более изящно и содержит меньше повторов кода
for x in range(1, 100):
    s = '';
    if x % 3 == 0:
        s += 'Fizz'
    if x % 5 == 0:
        s += "Buzz"
    if s == '':
        s = x
    print(s, end=' ')
"""