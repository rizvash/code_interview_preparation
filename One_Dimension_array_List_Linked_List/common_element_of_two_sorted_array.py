from typing import List


def find_common_elements(list1, list2):
    """ Func description complicity O(max(n,m))"""
    p1 = 0
    p2 = 0
    result = []
    while p1 < len(list1) and p2 < len(list2):
        if list1[p1] == list2[p2]:
            result.append(list1[p1])
            p1 += 1
            p2 += 1
        elif list1[p1] > list2[p2]:
            p2 += 1
        else:
            p1 += 1
    return result


if __name__ == '__main__':

    list_a1: List[int] = [1, 3, 4, 6, 7, 9]
    list_a2: List[int] = [1, 2, 4, 5, 9, 10]

    test1 = find_common_elements(list_a1, list_a2)
    assert test1 == [1, 4, 9]
#V2 intersection
    list1 = [1, 2, 3, 4, 5, 6]
    list2 = [3, 5, 7, 9]
    print(list(set(list1).intersection(list2)))
#V3 List comprishantion
    def common_elements(list1, list2):
        return [element for element in list1 if element in list2]

#v4
    A = [1, 2, 3, 4]
    B = [2, 4, 7, 8]
    commonalities = set(A) - (set(A) - set(B))
# common_elements(list_a1, list_a2) should return [1, 4, 9] (a list).

list_b1 = [1, 2, 9, 10, 11, 12]
list_b2 = [0, 1, 2, 3, 4, 5, 8, 9, 10, 12, 14, 15]
# common_elements(list_b1, list_b2) should return [1, 2, 9, 10, 12] (a list).

list_c1 = [0, 1, 2, 3, 4, 5]
list_c2 = [6, 7, 8, 9, 10, 11]
# common_elements(list_b1, list_b2) should return [] (an empty list).
