# redis
* [redis-py lib](https://redis-py.readthedocs.io/en/latest/genindex.html)
* [Related commands list](https://redis.io/commands/keys)
* [Python Redis & Redis Python Client](https://redislabs.com/lp/python-redis/)

*  -$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$--$-$-$-$-$-$-

* [redislabs](https://university.redislabs.com)
* [redis-best-pratices](https://redislabs.com/redis-best-practices)

```sh
docker run --rm -it -p 6379:6379 redis
```

* [awesome-cheatsheets](https://github.com/LeCoupa/awesome-cheatsheets/blob/master/databases/redis.sh)

## RU101 - Redis Data Structures

* [redis data-types](https://redis.io/topics/data-types)
* [data-types-intro](https://redis.io/topics/data-types-intro)

### Keys

* [commands](https://redis.io/commands#generic)
* [glob (programming)](https://en.wikipedia.org/wiki/Glob_(programming))

* Unique
* Binary Safe
* The maximum allowed key size is 512 MB
* Case sensitive

[SET](https://redis.io/commands/set)

`SET key value [EX seconds|PX milliseconds] [NX|XX] [KEEPTTL]`

```sh
redis> SET mykey "Hello"
"OK"
redis> GET mykey
"Hello"
redis> SET anotherkey "will expire in a minute" EX 60
"OK"
redis>
```

[GET](https://redis.io/commands/get)

`GET key`

```sh
redis> GET nonexisting
(nil)
redis> SET mykey "Hello"
"OK"
redis> GET mykey
"Hello"
redis>
```

[SCAN](https://redis.io/commands/scan)

`SCAN cursor [MATCH pattern] [COUNT count] [TYPE type]`

```sh
SET mykey:100 "Hello"
SET mykey:101 "Hello1"
SET mykey:102 "Hello2"
SET mykey:103 "Hello3"

localhost:6379> KEYS *
1) "mykey:101"
2) "mykey:103"
3) "mykey:102"
4) "mykey:100"
localhost:6379>

localhost:6379> scan 0
1) "0"
2) 1) "mykey:101"
   2) "mykey:103"
   3) "mykey:102"
   4) "mykey:100"
localhost:6379>

localhost:6379> scan 0 MATCH mykey:1* COUNT 10
1) "0"
2) 1) "mykey:101"
   2) "mykey:103"
   3) "mykey:102"
   4) "mykey:100"
localhost:6379> scan 0 MATCH mykey:1* COUNT 2
1) "1"
2) 1) "mykey:101"
   2) "mykey:103"
localhost:6379> scan 1 MATCH mykey:1* COUNT 10
1) "0"
2) 1) "mykey:102"
   2) "mykey:100"
localhost:6379>
```

[DEL](https://redis.io/commands/del)

* Blocking command

`DEL key [key ...]`

```sh
redis> SET key1 "Hello"
"OK"
redis> SET key2 "World"
"OK"
redis> DEL key1 key2 key3
(integer) 2
redis>
```

[UNLINK](https://redis.io/commands/unlink)

* non-Blocking command

```sh
redis> SET key1 "Hello"
"OK"
redis> SET key2 "World"
"OK"
redis> UNLINK key1 key2 key3
(integer) 2
redis>
```

[EXISTS](https://redis.io/commands/exists)

```sh
redis> SET key1 "Hello"
"OK"
redis> EXISTS key1
(integer) 1
redis> EXISTS nosuchkey
(integer) 0
redis> SET key2 "World"
"OK"
redis> EXISTS key1 key2 nosuchkey
(integer) 2
redis>
```

### Key Expiration

#### Set Expiration

[EXPIRE](https://redis.io/commands/expire)

`EXPIRE key seconds`

```sh
redis> SET mykey "Hello"
"OK"
redis> EXPIRE mykey 10
(integer) 1
redis> TTL mykey
(integer) 10
redis> SET mykey "Hello World"
"OK"
redis> TTL mykey
(integer) -1
redis>
```

[EXPIREAT](https://redis.io/commands/expireat)

`EXPIREAT key timestamp`

```sh
redis> SET mykey "Hello"
"OK"
redis> EXISTS mykey
(integer) 1
redis> EXPIREAT mykey 1293840000
(integer) 1
redis> EXISTS mykey
(integer) 0
redis>
```

[PEXPIRE](https://redis.io/commands/pexpire)

`PEXPIRE key milliseconds`

```sh
redis> SET mykey "Hello"
"OK"
redis> PEXPIRE mykey 1500
(integer) 1
redis> TTL mykey
(integer) 1
redis> PTTL mykey
(integer) 1499
redis>
```

[PEXPIREAT](https://redis.io/commands/pexpireat)

`PEXPIREAT key milliseconds-timestamp`

```sh
redis> SET mykey "Hello"
"OK"
redis> PEXPIREAT mykey 1555555555005
(integer) 1
redis> TTL mykey
(integer) -2
redis> PTTL mykey
(integer) -2
redis>
```

#### Inspect Expiration

[PTTL](https://redis.io/commands/pttl)

* TTL in milliseconds, or a negative value in order to signal an error (see the description above).

`PTTL key`

```sh
redis> SET mykey "Hello"
"OK"
redis> EXPIRE mykey 1
(integer) 1
redis> PTTL mykey
(integer) 999
redis>
```

[TTL](https://redis.io/commands/ttl)

`TTL key`

```sh
redis> SET mykey "Hello"
"OK"
redis> EXPIRE mykey 10
(integer) 1
redis> TTL mykey
(integer) 10
redis>
```

#### Remove Expiration

[PERSIST](https://redis.io/commands/persist)

`PERSIST key`

```sh
redis> SET mykey "Hello"
"OK"
redis> EXPIRE mykey 10
(integer) 1
redis> TTL mykey
(integer) 10
redis> PERSIST mykey
(integer) 1
redis> TTL mykey
(integer) -1
redis>
```

### Strings

* [commands#string](https://redis.io/commands#string)
* [TYPE](https://redis.io/commands/type)
* [object](https://redis.io/commands/object)

[TYPE](https://redis.io/commands/type)

`TYPE key`

```sh
redis> SET key1 "value"
"OK"
redis> LPUSH key2 "value"
(integer) 1
redis> SADD key3 "value"
(integer) 1
redis> TYPE key1
"string"
redis> TYPE key2
"list"
redis> TYPE key3
"set"
redis>
```

[OBJECT](https://redis.io/commands/object)

`OBJECT subcommand [arguments [arguments ...]]`

```sh
redis> lpush mylist "Hello World"
(integer) 4
redis> object refcount mylist
(integer) 1
redis> object encoding mylist
"ziplist"
redis> object idletime mylist
(integer) 10
```

### Hashes

* [HASH](https://redis.io/commands#hash)
* [GLOB](https://en.wikipedia.org/wiki/Glob_(programming))

[HSET](https://redis.io/commands/hset)

`HSET key field value [field value ...]`

```sh
redis> HSET myhash field1 "Hello"
(integer) 1
redis> HGET myhash field1
"Hello"
redis>
```

### Lists

* [LIST](https://redis.io/commands#list)
* [Doubly_linked_list](https://en.wikipedia.org/wiki/Linked_list#Doubly_linked_list)
