import itertools


def get_cost_path(rabbits, matrix_of_distance):
    cost = 0
    for i in range(0, len(rabbits) - 1):
        _from = rabbits[i]
        _to = rabbits[i + 1]
        cost += matrix_of_distance[_from][_to]
    start_node = 0
    end_node = len(matrix_of_distance) - 1
    cost += matrix_of_distance[start_node][rabbits[0]]
    cost += matrix_of_distance[rabbits[-1]][end_node]
    return cost


def compl_bell_ford_algo(edges):
    matrix_of_distance = []
    for vertex in range(len(edges)):
        distances = bell_ford_algo(edges, vertex)
        matrix_of_distance.append(distances)
    return matrix_of_distance


def bell_ford_algo(edges, start):
    distances = [float('inf') for vertex in edges]
    distances[start] = edges[start][start]
    for i in range(len(edges)):
        for u, v, weight in graph_edgs_enumerate(edges):
            if distances[u] + weight < distances[v]:
                distances[v] = distances[u] + weight
    return distances


def graph_edgs_enumerate(edges):
    for u, row in enumerate(edges):
        for v, weight in enumerate(row):
            yield (u, v, weight)


def h_negative_cycle(bellmford_matrix):
    distances = bellmford_matrix[0]
    for u, v, weight in graph_edgs_enumerate(bellmford_matrix):
        if distances[u] + weight < distances[v]:
            return True
    return False

def solution(times, time_limit):
    number_of_rabbits = len(times) - 2
    rabbit_indices = [rabbit + 1 for rabbit in range(number_of_rabbits)]

    matrix_of_distance = compl_bell_ford_algo(times)
    if h_negative_cycle(matrix_of_distance):
        return range(number_of_rabbits)

    for width in range(number_of_rabbits, 0, -1):
        for perm in itertools.permutations(rabbit_indices, width):
            cost = get_cost_path(perm, matrix_of_distance)
            if cost <= time_limit:
                return [rabbit - 1 for rabbit in sorted(perm)]

    return []

def test(times, budget, expected):
    answer = solution(times, budget)
    print(expected, answer, answer == expected)


cases = [
    ([[0, 1, 1, 1, 1], [1, 0, 1, 1, 1], [1, 1, 0, 1, 1], [1, 1, 1, 0, 1], [1, 1, 1, 1, 0]],
     3, [0, 1]),
    ([[0, 2, 2, 2, -1], [9, 0, 2, 2, -1], [9, 3, 0, 2, -1], [9, 3, 2, 0, -1], [9, 3, 2, 2, 0]],
     1, [1, 2])
]


def main():
    for times, budget, expected in cases:
        test(times, budget, expected)


main()