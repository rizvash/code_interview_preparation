
#### Assert
"assert \<condition>,\<error message>"

Exampl: assert False, "Oh no! This assertion failed!" 

    if not expression1:
        raise AssertionError(expression2)
        
     # using assert to check for 0 
     print ("The value of a / b is : ") 
     assert b != 0, "Divide by 0 error"
     print (a / b) 